using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IVerticeSupplier 
{
    void GenerateMesh(int Horizontal, int Verticle,float sideLength);
    ref List<VertexPlus> getVetices();
    void UpdateMesh(List<VertexPlus> VertexList);

    Vector2 getScale();

}
