using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour, IVerticeSupplier
{
     int Squares_Horizontal =10;
     int Squares_verticle = 5;
     float Squares_width = 1;
  

    [SerializeField] Vector3 TopLeftCorner;
    public int[] Triangles;
    Vector3[] vertices;
    public List<VertexPlus> Vertices;
    public List<Triangle> TrianglesList;
    public Mesh mesh;
    void generateVertices()
    {

        int verticesHorizontal = Squares_Horizontal + 1;
        int verticesveticle = Squares_verticle + 1;

        Vertices = new List<VertexPlus>();
        for (int j = 0; j < verticesveticle; j++)
        {
            for (int i = 0; i < verticesHorizontal; i++)
            {
               

                Vertices.Add(new VertexPlus(
                    TopLeftCorner   + 
                    i*Squares_width*Vector3.right   +
                    j*Squares_width*Vector3.down
                    ,Vector3.one));
            }
        }
    }

    void GenerateTriangles()
    {

        int verticesHorizontal = Squares_Horizontal + 1;
        int verticesveticle = Squares_verticle + 1;

        TrianglesList = new List<Triangle>();
        for (int j = 1; j < verticesveticle; j++)
        {
            for (int i = 1; i < verticesHorizontal; i++)
            {
   
                    TrianglesList.Add(
                        new Triangle(Vertices[j * verticesHorizontal + i], 
                                     Vertices[(j-1) * verticesHorizontal + i - 1], 
                                     Vertices[(j - 1) * verticesHorizontal + i]));
                    TrianglesList.Add(
                        new Triangle(Vertices[j * verticesHorizontal + i],
                                     Vertices[j * verticesHorizontal  + i-1 ],
                                     Vertices[(j - 1) * verticesHorizontal + i - 1]
                                     ));
            }
        }
    }
    [Button]
    public void Run() {
          generateVertices();
          GenerateTriangles();
          GenerateMesh();
        //generateSimple(); 

    }
    public void GenerateMesh()
    {
        int verticesHorizontal = Squares_Horizontal + 1;
        int verticesveticle = Squares_verticle + 1;

        mesh = new Mesh();
        mesh.Clear();
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.name = "Cloth";
           
            Triangles = new int[TrianglesList.Count * 3];
           Vector3[] vertices = new Vector3[Vertices.Count];
           Vector2[] uv = new Vector2[Vertices.Count];
        for (int i = 0; i < Vertices.Count; i++)
            {
                vertices[i] = Vertices[i].position;
                uv[i].x =  ((float) i % verticesHorizontal)  / verticesHorizontal  ;
            uv[i].y = (float)((i / verticesHorizontal)) / verticesveticle;
           

        }

            for (int i = 0; i < TrianglesList.Count; i++)
            {
                Triangles[i * 3] = Vertices.IndexOf(TrianglesList[i].Vertexes[0]);
                Triangles[1 + i * 3] = Vertices.IndexOf(TrianglesList[i].Vertexes[1]);
                Triangles[2 + i * 3] = Vertices.IndexOf(TrianglesList[i].Vertexes[2]);
            }

        mesh.vertices = vertices;
        mesh.triangles = Triangles;
        mesh.uv = uv;
        mesh.RecalculateNormals();
    }

    private void OnDrawGizmos()
    {
        
        for (int i = 0; i < Vertices.Count; i++)
        {
            if(Vertices[i].locked)
                Gizmos.color = Color.red;
            else
                Gizmos.color = Color.green;

            Gizmos.DrawSphere(Vertices[i].position, .2f);
        }
    }

    public void GenerateMesh(int Horizontal, int Verticle,float sideLength)
    {
        Squares_Horizontal = Horizontal;
        Squares_verticle = Verticle;
        Squares_width = sideLength;
        Run();
    }

    public ref List<VertexPlus> getVetices()
    {
        return ref Vertices;
    }

    public void UpdateMesh(List<VertexPlus> VertexList)
    {
        vertices = new Vector3[Vertices.Count];
        for (int i = 0; i < VertexList.Count; i++)
        {
           vertices[i] = VertexList[i].position;
        }
        mesh.vertices = vertices;
        mesh.triangles = Triangles;

    }

    public Vector2 getScale()
    {
        return new Vector2(Squares_Horizontal + 1, Squares_verticle + 1);
    }
}

[System.Serializable]
public class VertexPlus {
    public Vector3 position;
    public Vector3 old_position;
    public Vector3 normal;
    public bool locked;
   

    public VertexPlus(Vector3 position, Vector3 normal)
    { 
        this.position = position;
        this.old_position = position;
        this.normal = normal;   
      
    }
}

[System.Serializable]
public class Triangle
{ 
    public VertexPlus[] Vertexes = new VertexPlus[3];
    public Triangle(VertexPlus Point1 , VertexPlus Point2 , VertexPlus Point3)
    {
        Vertexes[0] = Point1;
        Vertexes[1] = Point2;   
        Vertexes[2] = Point3;
    }

}
