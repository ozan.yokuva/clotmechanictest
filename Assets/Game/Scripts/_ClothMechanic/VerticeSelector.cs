using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticeSelector : MonoBehaviour
{

    IVerticeSupplier verticeSupplier;
    bool[] locked;
    List<VertexPlus> vertices;
    Vector2 ClothScale;
    int selected = 0;
    void Start()
    {
        
        verticeSupplier = GetComponent<IVerticeSupplier>();
        vertices = verticeSupplier.getVetices();
        locked = new bool[vertices.Count];
        ClothScale = verticeSupplier.getScale();

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
            vertices[selected].locked = !vertices[selected].locked;
        if (Input.GetKeyDown(KeyCode.RightArrow))
            selected++;
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            selected--;
        if (Input.GetKeyDown(KeyCode.DownArrow))
            selected += (int)ClothScale.x;
        if (Input.GetKeyDown(KeyCode.UpArrow))
            selected -= (int)ClothScale.x;


        selected = Mathf.Clamp(selected, 0, vertices.Count-1);

        verticeSupplier.UpdateMesh(vertices);

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        if(vertices!=null && vertices.Count> selected)
            Gizmos.DrawSphere(vertices[selected].position, .4f);
    }


}
