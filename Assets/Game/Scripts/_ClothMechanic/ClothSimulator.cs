using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothSimulator : MonoBehaviour
{
    [SerializeField] int _Horizontal;
    [SerializeField] int _Vertical;
    [SerializeField] float _Gravity;
    [SerializeField] int _Iterations = 3;
    [SerializeField] float _SquareSide;
    [SerializeField] bool _Simulate;
    Vector3 _OriginalPos;
    Vector3 _tempVector;
    Vector3 _PositionDifference {
        get
        {
            return transform.position - _OriginalPos;
        }
    }
    
    IVerticeSupplier _VerticeSupplier;
    Stick[] _Sticks;
    List<Stick> _StickList;
    List<VertexPlus> _VertexPlusList;



    void Start()
    {
        _VerticeSupplier = GetComponent<IVerticeSupplier>();
        CreateVertices();
        createSticks();
    }

    public void CreateVertices (){ 
    
        _VerticeSupplier.GenerateMesh(_Horizontal, _Vertical,_SquareSide);
    }
    public void createSticks() {
        _VertexPlusList = _VerticeSupplier.getVetices();
        _StickList = new List<Stick>(); 
        for (int j = 0; j < _Vertical; j++)
        {
            for (int i = 0; i < _Horizontal; i++)
            {
               
                _StickList.Add(
                    new Stick(_VertexPlusList[j * (_Horizontal+1) + i],
                              _VertexPlusList[j * (_Horizontal+1) + i + 1])
                    );
                _StickList.Add(
                   new Stick(_VertexPlusList[j * (_Horizontal+1) + i],
                             _VertexPlusList[(j+1) * (_Horizontal+1) + i ])
                   );
            }
        }
        //Add The LastSticks Bottom&right;
        for (int i = 0; i < _Vertical ; i++)
        {
            _StickList.Add(
            new Stick(_VertexPlusList[(_Horizontal + 1) * i  + _Horizontal ],
                      _VertexPlusList[(_Horizontal + 1) * (i+1) + _Horizontal] )
            );
        }
        for (int i = 0; i < _Horizontal ; i++)
        {
            _StickList.Add(new Stick(_VertexPlusList[(_Horizontal + 1)*_Vertical + i],
                          _VertexPlusList[(_Horizontal + 1) * _Vertical + i+1]));
        }
        _Sticks = _StickList.ToArray();
    }

    public void Simulate(float time)
    {

        for (int i = 0; i < _VertexPlusList.Count; i++)
        {
            if (!_VertexPlusList[i].locked)
            {
                _tempVector = _VertexPlusList[i].position;
                _VertexPlusList[i].position += (_VertexPlusList[i].position - _VertexPlusList[i].old_position);
                _VertexPlusList[i].position += Vector3.down * _Gravity * time * time;
                _VertexPlusList[i].old_position = _tempVector;
            }
            else
            {
                _VertexPlusList[i].position = _VertexPlusList[i].old_position + _PositionDifference;
            }

        }

        for (int j = 0; j < _Iterations; j++)
        {
            for (int i = 0; i < _Sticks.Length; i++)
            {
                _Sticks[i].Reposition();

            }
        }
    }

    void LateUpdate()
    {
        if (_Simulate)
        {
            Simulate(Time.deltaTime);
            _VerticeSupplier.UpdateMesh(_VertexPlusList);
        }
    }

    private void OnDrawGizmos()
    {
        if(_Sticks!=null)
        for (int i = 0; i < _Sticks.Length; i++)
        { 
        Gizmos.color = Color.blue;
            Gizmos.DrawLine(_Sticks[i].Point1.position, _Sticks[i].Point2.position);
        }
    }
}
public class Stick {

    Vector3 _Center,_Direction;
    public Stick(VertexPlus p1 , VertexPlus p2)
    {
        this.Point1 = p1;
        this.Point2 = p2;
        this.Lenght = Vector3.Distance(p1.position, p2.position);
    }
    public VertexPlus Point1;
    public VertexPlus Point2;
    public float Lenght;

    public void Reposition()
    {

        _Center = (Point1.position + Point2.position) / 2;
        _Direction = (Point1.position - Point2.position).normalized;
        if(!Point1.locked)
            Point1.position = _Center + _Direction * Lenght /2;

        if (!Point2.locked)
            Point2.position = _Center - _Direction * Lenght / 2;
    }

    
}

