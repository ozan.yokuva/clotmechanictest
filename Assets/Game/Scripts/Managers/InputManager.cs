using EC.Managers.Events;
using UnityEngine;

namespace EC.Managers
{
    public class InputManager : MonoBehaviour
    {
        public float sensitivity;
        private Vector3 lastMousePosition;
        private bool pointerActive = false;
        private Vector3Args axisArgs = new Vector3Args();

        public bool PointerActive { get => pointerActive; }
        public Vector3Args AxisArgs { get => axisArgs; }
        public void Initialize()
        {
            sensitivity /= Screen.width;
        }
        private void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                lastMousePosition = Input.mousePosition;
                pointerActive = true;
                EventRunner.HoldStart(new Vector3Args(Input.mousePosition));
            }
            else if(Input.GetMouseButtonUp(0))
            {
                pointerActive = false;
                EventRunner.HoldFinish(new Vector3Args(Input.mousePosition));
            }
            if(pointerActive)
            {
                axisArgs.x = (Input.mousePosition.x - lastMousePosition.x) * sensitivity;
                axisArgs.y = (Input.mousePosition.y - lastMousePosition.y) * sensitivity;
                EventRunner.Stationary(axisArgs);
            }
            lastMousePosition = Input.mousePosition;
        }

        public void DeactivateInput()
        {
            axisArgs.Reset();
            pointerActive = false;
        }
    }
}

