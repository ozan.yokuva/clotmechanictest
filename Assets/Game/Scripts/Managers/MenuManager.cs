﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using EC.Utility;
using EC.Managers.Menu;
using TMPro;
using EC.Managers.Events;

namespace EC.Managers
{
	public class MenuManager : MonoBehaviour 
	{
		[SerializeField] private GamePanel gamePanel;
		[SerializeField] private LoadingPanel loadingPanel;
		[SerializeField] private PanelManager failPanel;
		[SerializeField] private PanelManager successPanel;
		[SerializeField] private GameObject tutorial;

		private bool youWin;
		private bool gameOver;
		private bool isInitialized;

        public GamePanel GamePanel { get => gamePanel; }
        public PanelManager FailPanel { get => failPanel; }
        public PanelManager SuccessPanel { get => successPanel; }

        private void Awake()
        {
			tutorial.SetActive(false);
			//SceneManager.LoadScene("ManagerScene", LoadSceneMode.Additive);
		}
        public void Initialize()
		{
			if(!isInitialized)
            {
				gamePanel.Initialize();
				failPanel.Initialize();
				successPanel.Initialize();
				loadingPanel.Initialize();
				MainManager.Instance.EventManager.Register(EventTypes.LevelStart, LevelStart);
				MainManager.Instance.EventManager.Register(EventTypes.LevelSuccess, LevelSuccess);
				MainManager.Instance.EventManager.Register(EventTypes.LevelFail, LevelFail);
				MainManager.Instance.EventManager.Register(EventTypes.LevelRestart, LevelRestart);
				MainManager.Instance.EventManager.Register(EventTypes.LevelFinish, LevelFinish);
				MainManager.Instance.EventManager.Register(EventTypes.LoadSceneStart, LoadingStart);
				MainManager.Instance.EventManager.Register(EventTypes.LoadSceneFinish, LoadingFinish);
				MainManager.Instance.EventManager.Register(EventTypes.LevelLoaded, LevelLoaded);
				isInitialized = true;
			}
		}

		public void LevelLoaded(EventArgs args)
        {
			tutorial.SetActive(true);
			gamePanel.levelText.text = (PlayerPrefs.GetInt("Level") + 1).ToString();
			gamePanel.nextLevelText.text = (PlayerPrefs.GetInt("Level") + 2).ToString();
		}

		public void LevelStart(EventArgs args)
		{
			gamePanel.restartButton.SetActive(true);
			tutorial.SetActive(false);
			youWin = false;
			gameOver = false;
		}

		public void LevelFinish(EventArgs args)
        {
			tutorial.SetActive(false);
			StartCoroutine(PostLevelRestart());
        } 

		public void LevelFail(EventArgs args)
		{
			Debug.Log("Fail");
			if (youWin || gameOver)
			{
				return;
			}
			gameOver = true;
			gamePanel.restartButton.SetActive(false);
			Delayer.Delay(MainManager.Instance.EditorGameSettings.failPanelAppearTime, () =>
			{
				gamePanel.Disappear();
				failPanel.Appear();
			});
		}

		public void LevelSuccess(EventArgs args)
		{
			Debug.Log("Success");
			if (youWin || gameOver)
			{
				return;
			}
			youWin = true;
			gamePanel.restartButton.SetActive(false);
			Delayer.Delay(MainManager.Instance.EditorGameSettings.successPanelAppearTime, () =>
			{
				gamePanel.Disappear();
				successPanel.Appear();
			});
		}

		public void LevelRestart(EventArgs args)
        {
			StartCoroutine(PostLevelRestart());
        }

		public IEnumerator PostLevelRestart()
        {
			yield return new WaitForSeconds(.5f);
			tutorial.SetActive(true);
			failPanel.Disappear();
			successPanel.Disappear();
			gamePanel.Appear();
		}

		public void LoadingStart(EventArgs args)
        {
			loadingPanel.Appear(args);
        }

		public void LoadingFinish(EventArgs args)
        {
			loadingPanel.Disappear();
        }
	}
	public enum ControllerType
	{
		Tap,
		HoldDown,
		Swipe,
		Swerve,
		HorizontalSwerve,
	}
}



