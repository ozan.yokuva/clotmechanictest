using System.Collections.Generic;
using UnityEngine;
using EC.Data;

[CreateAssetMenu(fileName = "AssetManager", menuName = "Scriptlable Objects/Asset Manager")]

public class AssetManager : ScriptableObject
{
    [SerializeField] private DictionaryData<Sprite>[] spriteData;
    [SerializeField] private DictionaryData<GameObject>[] prefabData;
    [SerializeField] private DictionaryData<Material>[] materialData;
    [SerializeField] private DictionaryData<Texture2D>[] textureData;

    private Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
    private Dictionary<string, GameObject> gameObjects = new Dictionary<string, GameObject>();
    private Dictionary<string, Material> materials = new Dictionary<string, Material>();
    private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();

    public void Initialize()
    {
        //Putting all data into dictionarires to be faster. We lose memory and earn speed
        for (int i = 0; i < spriteData.Length; i++)
        {
            sprites.Add(spriteData[i].key, spriteData[i].value);
        }
        for (int i = 0; i < prefabData.Length; i++)
        {
            gameObjects.Add(prefabData[i].key, prefabData[i].value);
        }
        for (int i = 0; i < materialData.Length; i++)
        {
            materials.Add(materialData[i].key, materialData[i].value);
        }
        for (int i = 0; i < textureData.Length; i++)
        {
            textures.Add(textureData[i].key, textureData[i].value);
        }
    }

    public Sprite GetSprite(string key)
    {
        if (sprites.ContainsKey(key))
        {
            return sprites[key];
        }
        return null;
    }

    public GameObject GetPrefab(string key)
    {
        if (gameObjects.ContainsKey(key))
        {
            return gameObjects[key];
        }
        return null;
    }

    public Material GetMaterial(string key)
    {
        if (materials.ContainsKey(key))
        {
            return materials[key];
        }
        return null;
    }

    public Texture2D GetTexture2D(string key)
    {
        if (textures.ContainsKey(key))
        {
            return textures[key];
        }
        return null;
    }
}
