using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;

namespace EC.Managers.Menu
{
    public class CurrencyUI : MonoBehaviour
    {
        public TextMeshProUGUI currencyAmountText;
        public Image currencyImage;
        public Animator currencyAnimator;

        private string currencySign;
        private int currencyAmount;
        private Coroutine incrementRoutine;

        public void Initialize(Sprite currencyIcon, string currencySign, int currencyAmount)
        {
            this.currencyImage.sprite = currencyIcon;
            this.currencySign = currencySign;
            SetCurrency(currencyAmount);
        }
        public void SetCurrency(int nextAmount)
        {
            if (currencyAmount < nextAmount)
            {
                currencyAnimator.Play("Earn");
            }
            else if (currencyAmount > nextAmount)
            {
                currencyAnimator.Play("Spend");
            }
            currencyAmount = nextAmount;
            currencyAmountText.text = nextAmount + "" + currencySign;
        }

        public void SetCurrencyIncremental(int nextAmount)
        {
            if(incrementRoutine != null)
            {
                StopCoroutine(incrementRoutine);
            }
            currencyAnimator.Play("Earn");
            incrementRoutine = StartCoroutine(MoneyChangeRoutine(nextAmount));
        }

        public IEnumerator MoneyChangeRoutine(int nextAmount)
        {
            float lerpValue = 0;
            while(lerpValue < 1)
            {
                currencyAmount = (int)Mathf.Lerp(currencyAmount, nextAmount, lerpValue);
                currencyAmountText.text = currencyAmount + currencySign;
                lerpValue += Time.deltaTime * 2f;
                yield return null;
            }
            currencyAnimator.Play("Empty");
            SetCurrency(nextAmount);
        }
    } 
}

