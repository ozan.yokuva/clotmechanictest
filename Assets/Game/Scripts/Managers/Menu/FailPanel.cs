using EC.Managers.Events;

namespace EC.Managers.Menu
{
    public class FailPanel : PanelManager
    {
        public void HandleRestartButton()
        {
            Disappear();
            EventRunner.LoadSceneStart();
        }
    }
}
