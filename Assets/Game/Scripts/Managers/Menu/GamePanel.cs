using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;
using EC.Utility;
using System;
using EC.Managers.Events;
using EC.Managers.Currencies;
using UnityEngine;
using System.Linq;
using System.Collections;

namespace EC.Managers.Menu
{
    public class GamePanel : PanelManager
    {
        public GameObject restartButton;
        public GameObject[] tutorialPanels;
		public TextMeshProUGUI levelText;
		public TextMeshProUGUI nextLevelText;
		public List<CurrencyUI> currencyPanels = new List<CurrencyUI>();
        public FillPanel progressBarFillImage;
        public Image vibImage;
        public Transform currencyParent;

        public override void Initialize()
        {
            base.Initialize();
            Appear();
            tutorialPanels[(int)MainManager.Instance.EditorGameSettings.controllerType].SetActive(true);
            MainManager.Instance.EventManager.Register(EventTypes.VibrationChange, VibChange);
            MainManager.Instance.EventManager.Register(EventTypes.CurrencySpent, SpendMoney);
            MainManager.Instance.EventManager.Register(EventTypes.CurrencyEarned, SpendMoney);
            MainManager.Instance.EventManager.Register(EventTypes.LevelStart, LevelStart);
            MainManager.Instance.EventManager.Register(EventTypes.LevelFinish, LevelRestart);
            MainManager.Instance.EventManager.Register(EventTypes.LevelRestart, LevelRestart);
            MainManager.Instance.EventManager.Register(EventTypes.LevelLoaded, LevelLoaded);
            Currency[] currencies = MainManager.Instance.CurrencyManager.currencies;
            GameObject currencyPrefab = MainManager.Instance.AssetManager.GetPrefab("CurrencyUI");
            for (int i = 0; i < currencies.Length; i++)
            {
                currencyPanels.Add(Instantiate(currencyPrefab, currencyParent.position, currencyParent.rotation).GetComponent<CurrencyUI>());
                currencyPanels[i].transform.SetParent(currencyParent);
                currencyPanels[i].Initialize(currencies[i].currencyImage, currencies[i].currencySign, currencies[i].currencyAmount);
            }
        }

        public void HandleVibChangeButton()
        {
            MainManager.Instance.SettingsManager.ChangeVib(!MainManager.Instance.SettingsManager.VibOn);
        }

        private void VibChange(EventArgs myBool)
        { 
            bool vibOn = (myBool as BoolArgs).value;
            vibImage.sprite = vibOn ? MainManager.Instance.AssetManager.GetSprite("VibOn") : MainManager.Instance.AssetManager.GetSprite("VibOff");
        }

        public void SpendMoney(EventArgs cData)
        {
            var currencyData = cData as CurrencyArgs;
            Currency currency = MainManager.Instance.CurrencyManager.currencies[currencyData.currencyId];
            if(currencyData.addIncrementally)
            {
                currencyPanels[currencyData.currencyId].SetCurrencyIncremental(currency.currencyAmount);
            }
            else
            {
                currencyPanels[currencyData.currencyId].SetCurrency(currency.currencyAmount);
            }
        }

        public void LevelRestart(EventArgs args)
        {
            restartCooldown = true;
            tutorialPanels[(int)MainManager.Instance.EditorGameSettings.controllerType].SetActive(true);
        }

        public void LevelStart(EventArgs cData)
        {
            for (int i = 0; i < tutorialPanels.Length; i++)
            {
                tutorialPanels[i].SetActive(false);
            }
        }

        public void LevelLoaded(EventArgs args)
        {
            restartCooldown = false;
            Currency[] currencies = MainManager.Instance.CurrencyManager.currencies;
            for (int i = 0; i < currencies.Length; i++)
            { 
               currencyPanels[i].SetCurrency(currencies[i].currencyAmount);
            }
        }

        public void HandleStartPlaying()
        {
            EventRunner.LevelStart();
        }

        private bool restartCooldown;
        private Coroutine restartCooldownRoutine;
        public void HandleRestartButton()
        {   
            if(!restartCooldown)
            {
                EventRunner.LoadSceneStart();
            }
        }
	}
}

