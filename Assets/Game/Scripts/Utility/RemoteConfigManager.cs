using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if ELEPHANT_SDK_MANAGER_IMPORTED
using ElephantSDK;
#endif


namespace EC.Utility
{
    public class RemoteConfigManager
    {
        public int GetIntConfig(string key, int defaultValue = 0)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            string value = RemoteConfig.GetInstance().Get(key, defaultValue.ToString());
            return int.Parse(value);
#else
            return defaultValue;
#endif
        }

        //Sometimes it is problematic upon systems
        public float GetFloatConfig(string key, float defaultValue = 0)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            string value = RemoteConfig.GetInstance().Get(key, defaultValue.ToString());
            return float.Parse(value, System.Globalization.CultureInfo.InvariantCulture);
#else
            return defaultValue;
#endif
        }

        public bool GetBoolConfig(string key, bool defaultValue = false)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            string value = RemoteConfig.GetInstance().Get(key, defaultValue.ToString());
            return bool.Parse(value);
#else
            return defaultValue;
#endif

        }

        public int[] GetIntArrayConfig(string key, int[] defaultValue)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            string value = RemoteConfig.GetInstance().Get(key, defaultValue.ToString());
            return Array.ConvertAll(value.Split(';'), int.Parse);
#else
            return defaultValue;
#endif
        }

        public float[] GetFloatArrayConfig(string key, float[] defaultValue)
        {
#if ELEPHANT_SDK_MANAGER_IMPORTED
            string value = RemoteConfig.GetInstance().Get(key, String.Join(";", defaultValue));
            return Array.ConvertAll(value.Split(';'), float.Parse);
#else
            return defaultValue;
#endif
        }

    }
}

