using UnityEngine.UI;

namespace EC.Utility
{
    public class FillImageFillPanel : FillPanel
    {
        public Image.FillMethod fillMethod = Image.FillMethod.Horizontal;

        public override void SetFill(float fillAmount)
        {
            base.SetFill(fillAmount);
            fillImage.fillAmount = fillAmount / maxValue;
        }
    }
}

