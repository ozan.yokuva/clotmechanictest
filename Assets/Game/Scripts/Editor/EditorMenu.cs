using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector.Editor;
using EC.Managers;
using EC.Data;
using EC.Utility;

namespace EC.Editor
{
    public class EditorMenu : OdinMenuEditorWindow
    {
        [MenuItem("EC Framework/Settings", false, 30)]
        private static void OpenWindow()
        {
            EditorWindow myWindow = GetWindow<EditorMenu>("EC Framework Settings");
            myWindow.minSize = new Vector2(100, 100);
        }

        protected override OdinMenuTree BuildMenuTree()
        {
            var tree = new OdinMenuTree();

            tree.AddAssetAtPath("Editor Game Settings", "Assets/Resources/Managers/Settings.asset", typeof(EditorGameSettings));
            tree.AddAssetAtPath("Event Holder", "Assets/Game/Scriptable Objects/EventCreator.asset", typeof(EnumObject));
            tree.AddAssetAtPath("Level Manager", "Assets/Game/Scriptable Objects/Managers/LevelManager.asset", typeof(LevelManager));
            tree.AddAssetAtPath("PList Adder", "Assets/Resources/Managers/PListChanger.asset", typeof(PListData));
            tree.AddAssetAtPath("Asset Manager", "Assets/Game/Scriptable Objects/Managers/AssetManager.asset", typeof(AssetManager));
            tree.AddAssetAtPath("Currency Manager", "Assets/Game/Scriptable Objects/Managers/CurrencyManager.asset", typeof(CurrencyManager));
            return tree;
        }
    }
}

